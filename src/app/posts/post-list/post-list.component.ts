import { Component, OnInit, Input, OnDestroy } from '@angular/core';
import {Post} from '../post.model';
import {PostService} from '../post.service';
import { Subscription } from 'rxjs';
import { AuthService } from 'src/app/auth/auth.service';

@Component({
  selector: 'app-post-list',
  templateUrl: './post-list.component.html',
  styleUrls: ['./post-list.component.css']
})
export class PostListComponent implements OnInit {

  constructor(public postservice: PostService , private authservice: AuthService) { }

  // posts = [
  //   {title: 'first', content : 'this is first conteny'},
  //   {title: 'second', content : 'this is second content'},
  //   {title: 'third', content : 'this is third content'},
  //   {title: 'four', content : 'this is fourth content'},
  // ];
posts: Post[] = [];
totalPosts = 10;
postsPerPage = 2;
pageSizeOptions = [1, 3, 5, 7];
private postSub: Subscription;
userIsAuthenticated = false;
private authListenerSubs: Subscription;

  ngOnInit(): void {
    this.postservice.getPost();
    this.postSub = this.postservice.getPostUpdatedListner()
    .subscribe((posts: Post[]) => {
      this.posts = posts;
    }) ;
    this.userIsAuthenticated = this.authservice.getIsAuth();
    this.authListenerSubs = this.authservice.getAuthStatusListener().subscribe(isAuthenticated => {
      this.userIsAuthenticated = isAuthenticated;
    });
  }

  onDelete(postId: string) {
    this.postservice.daletePost(postId);
  }

  // tslint:disable-next-line:use-lifecycle-interface
  ngOnDestroy() {
    this.postSub.unsubscribe();
    this.authListenerSubs.unsubscribe();
  }

}
