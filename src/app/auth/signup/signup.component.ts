import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { AuthService } from '../auth.service';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css']
})
export class SignupComponent implements OnInit {

  constructor(public authservice: AuthService) { }

  ngOnInit(): void {
  }

  onSignup(ngForm: NgForm) {
    if (ngForm.invalid) {
      return;
    }
    this.authservice.createUser(ngForm.value.email, ngForm.value.password);
  }

}
